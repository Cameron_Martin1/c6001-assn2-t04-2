# **_Milestone 1: Planning Report_**

## **_Jaskarn Singh, Sam Halligan, Cameron Martin_**

### **Our Project**

   > For our project we plan to make a simple web application. This application will pull information from Facebook. This means we will be using the Facebook API. As a group we decided this API was something we were interested in and wanted to learn more about. Especially since we are all advid users of Facebook. Further this gives us a chance to learn more about the code and processes used. In the Web design and application of Facebook.

### **Tools used and Why?**

#### **Facebook Messenger**

   > We will be using Facebook Messenger as our main form of communication. This is mainly due to the ease of use and accessibility. We all use messenger and always check it. Logically this is a better method then slack. However because we are required to use slack. I will make sure we copy paste all information and conversation about the group project to the slack channel.

#### **Slack**

   > Although I said Facebook Messenger would be our main form of communication. During class or times when we are at the ploytechnic we will make sure to use slack and share any relevant information/resources we find there. Slack has a lot of unique functions like being able to pin posts/links. We will be using this function to keep track of important information and resorces for our project.

### **Wireframe**

   > We have come up with a simple wireframe showcasing our application. We as a group acknowledge we are not so good with programming. So we decided to stick with a simplistic program that will cover the requirements of the assessment. Looking briefly over the funcions and coding required. And more at what we want out of the app. This relates to what we hope to achieve through this application.

#### **Key Information about the application**

1. Functions
2. Terms and Conditions
3. API Knowledge
4. ASP.NET CORE and API
5. Authorization
6. User Interface Considerations
7. Aesthetics 
8. Components
9. Updating data or refreshing

   > All the above will be researched and added to our collected information. Allowing any one of us to be able to complete any task. The main idea is we want this web application to appear professional. This is why we have gone with similar colourization and theme as Facebook itself. This serves as two purposes. One it allows the user to see clearly waht this application requires. While further attracting viewers with the layout.

#### **Planning and Key Components**

1. Researching the above 9 points. This will be delegated to the three of us. (This stage will occur before milestone 2 but is important relating to planning)
    1. Functions
    2. Terms and Conditions
    3. API Knowledge
    4. ASP.NET CORE and API
    5. Authorization
    6. User Interface Considerations
    7. Aesthetics 
    8. Components
    9. Updating data or refreshing
    10. Any Images or Aethetical information required
    11. Apply for Facebook Developer. Allowing us to use the API.
2. Starting off we will need to make each of the main three components. This relates to the two main pages and the pop-up page. We will split the coding for this between us.
    1. Create the Login Page.
    2. Create the Home Page.
    3. Create the Login Pop-up Page.
    4. Code is labelled and easy to follow. Notes included.
3. Next another one of us will make sure the images/layout of the pages match the wireframe.
    1. Compare Layouts to Web Application
    2. Check we collected all images and information relating to layout and aesthetics.
    3. Aesthetical Considerations.
    4. Images and Colour scheme will match facebook as per above.
    5. Confirm between all members.
5. Interactive buttons/links and making sure the web application flows and is functional.
    1. Login button is working as needed.
    2. Web application can login and logout.
    3. Web application signup link works for new users.
    4. Smooth transition between pages.
    5. Code is labelled and easy to follow. Notes included.
6. Code for the web app to be compatible on multiple platforms.
    1. Responsive web pages. Works on all platforms.
    2. tests on multiple platforms.
    3. Correcting any disortion.
    4. Code is labelled and easy to follow. Notes included.
7. Integrate Facebook API into the web app.
    1. API Coding setup. This relates to the Important code related to the Facebook API.
    2. Code to extract information from the user profile.
    3. Code is labelled and easy to follow. Notes included.
8. Incorporate key GUI elements into designing the Web app.
    1. These elements will be from our research.
    2. Apply key changes to make our web application more effective.
    3. Aesthetical considerations for GUI elements and design.
9. Data can be refreshed through the refresh button.
    1. Data refresh button is created.
    2. All code related to data being refreshed on the page.
    3. Code is labelled and easy to follow. Notes Included.

[Wireframe for our App](https://drive.google.com/a/bcs.net.nz/file/d/0B8YB6--euYpOTDkwUG9sbXVEZTA/view?usp=sharing)

### **Roles of Group Members**

